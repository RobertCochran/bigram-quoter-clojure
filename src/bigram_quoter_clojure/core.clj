(ns bigram-quoter-clojure.core
  (:gen-class))

(defn file-sentences
  [f]
  (clojure.string/split (clojure.string/replace (slurp f) #"\n" " ")
                        #"(?<=[.!?])[ ]*"))

(defn sentence-words
  [sentence]
  (into [:sentence-start]
        (clojure.string/split sentence #"[ ]|(?=[.!?])")))

(defn bigram-map
  [words]
  (reduce (fn [acc [keys value]]
            (assoc-in acc keys value))
          {}
          (frequencies (partition 2 1 words))))

;; https://clojuredocs.org/clojure.core/merge-with#example-5b80843ae4b00ac801ed9e74
(defn deep-merge-with
  "Like merge-with, but merges maps recursively, applying the given fn
  only when there's a non-map at a particular level.
  (deep-merge-with + {:a {:b {:c 1 :d {:x 1 :y 2}} :e 3} :f 4}
                     {:a {:b {:c 2 :d {:z 9} :z 3} :e 100}})
  -> {:a {:b {:z 3, :c 3, :d {:z 9, :x 1, :y 2}}, :e 103}, :f 4}"
  [f & maps]
  (apply
   (fn m [& maps]
     (if (every? map? maps)
       (apply merge-with m maps)
       (apply f maps)))
   maps))

(defn make-new-sentence
  [bigram-map]
  (loop [acc []
         current-word :sentence-start]
    (if (some #(= current-word %) #{"." "!" "?"})
      acc
      (let [next-word (rand-nth (mapcat (partial apply #(repeat %2 %1))
                                        (get bigram-map current-word)))]
        (recur (conj acc next-word)
               next-word)))))

(defn concat-sentence
  [sentence-words]
  (clojure.string/join [(clojure.string/join " " (butlast sentence-words))
                        (last sentence-words)]))

(defn generate-bigram-sentence
  [file]
  (concat-sentence (make-new-sentence
                    (apply deep-merge-with
                           +
                           (pmap (comp bigram-map sentence-words)
                                 (file-sentences file))))))

(defn -main
  "Print a generated bigram sentence."
  [& args]
  (println (generate-bigram-sentence (first args))))
